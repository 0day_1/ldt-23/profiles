package ru.day.ltd.profiles.data.enums;

public enum ProfileType {

    LANDLORD("LANDLORD"),
    RENTER("RENTER");

    private final String value;

    ProfileType(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }

    public String getAuthorityString() {
        return "ROLE_" + value;
    }

    public static ProfileType fromString(String value) throws IllegalArgumentException {
        for (ProfileType profileType : ProfileType.values()) {
            if (profileType.getValue().equals(value)) {
                return profileType;
            }
        }
        throw new IllegalArgumentException("No such value in UserType");
    }

}
