package ru.day.ltd.profiles.data;

import org.springframework.data.jpa.repository.JpaRepository;
import ru.day.ltd.profiles.data.entity.Profile;

import java.util.Optional;
import java.util.UUID;

public interface ProfileRepository extends JpaRepository<Profile, UUID> {

    Optional<Profile> findByUserId(UUID userId);



}
