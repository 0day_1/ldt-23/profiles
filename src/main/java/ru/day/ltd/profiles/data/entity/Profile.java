package ru.day.ltd.profiles.data.entity;

import lombok.Getter;
import lombok.NonNull;
import lombok.Setter;
import ru.day.ltd.profiles.data.dto.ProfileDTO;
import ru.day.ltd.profiles.data.enums.ProfileType;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;
import java.util.Objects;
import java.util.UUID;

@Entity
@Table(name = "PROFILE")
@Getter @Setter
public class Profile {

    @Id
    @NonNull
    @Column(name = "ID", nullable = false)
    private UUID id;

    @NonNull
    @Column(name = "USER_ID", nullable = false)
    private UUID userId;

    @NonNull
    @Column(name = "NAME", nullable = false)
    private String name;

    @Column(name = "EMAIL")
    private String email;

    @NonNull
    @Column(name = "PHONE", nullable = false)
    private String phone;

    @NonNull
    @Column(name = "TYPE", nullable = false)
    private String type;

    public ProfileType getProfileType() {
        return ProfileType.fromString(type);
    }

    public void setProfileType(ProfileType profileType) {
        ProfileDTO profileDTO = new ProfileDTO();
        this.type = Objects.requireNonNull(profileType).getValue();
    }

}
