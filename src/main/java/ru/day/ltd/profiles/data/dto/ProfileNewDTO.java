package ru.day.ltd.profiles.data.dto;

import lombok.*;

import java.util.UUID;

@Getter
@Setter
@NoArgsConstructor
@AllArgsConstructor
@EqualsAndHashCode
public class ProfileNewDTO {

    private UUID userId;

    private String name;

    private String email;

    private String phone;

    private String type;

}
