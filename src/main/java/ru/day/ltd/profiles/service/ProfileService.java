package ru.day.ltd.profiles.service;

import ru.day.ltd.profiles.data.dto.ProfileDTO;
import ru.day.ltd.profiles.data.dto.ProfileNewDTO;

import java.util.UUID;

public interface ProfileService {
    String NAME = "ProfileService";

    ProfileDTO getById(UUID id) throws IllegalArgumentException;

    ProfileDTO getByUserId(UUID userId) throws IllegalArgumentException;

    ProfileDTO createNewProfile(ProfileNewDTO profileNewDTO);
}
