package ru.day.ltd.profiles.service.impl;

import org.springframework.stereotype.Service;
import ru.day.ltd.profiles.data.ProfileRepository;
import ru.day.ltd.profiles.data.dto.ProfileDTO;
import ru.day.ltd.profiles.data.dto.ProfileNewDTO;
import ru.day.ltd.profiles.data.entity.Profile;
import ru.day.ltd.profiles.service.ProfileService;

import java.util.UUID;

@Service(ProfileService.NAME)
public class ProfileServiceImpl implements ProfileService {

    private final ProfileRepository profileRepository;

    public ProfileServiceImpl(ProfileRepository profileRepository) {
        this.profileRepository = profileRepository;
    }

    @Override
    public ProfileDTO getById(UUID id) throws IllegalArgumentException {
        if (id == null) {
            throw new IllegalArgumentException("id is null");
        }
        return profileRepository.findById(id)
                .map(this::toProfileDTO)
                .orElseThrow(() -> new IllegalArgumentException("No such profile"));
    }

    @Override
    public ProfileDTO getByUserId(UUID userId) throws IllegalArgumentException {
        if (userId == null) {
            throw new IllegalArgumentException("userId is null");
        }
        return profileRepository.findByUserId(userId)
                .map(this::toProfileDTO)
                .orElseThrow(() -> new IllegalArgumentException("No profile for userId: " + userId));
    }

    @Override
    public ProfileDTO createNewProfile(ProfileNewDTO profileNewDTO) {
        Profile profile = new Profile();
        profile.setUserId(profileNewDTO.getUserId());
        profile.setName(profileNewDTO.getName());
        profile.setEmail(profileNewDTO.getEmail());
        profile.setPhone(profileNewDTO.getPhone());
        profile.setType(profileNewDTO.getType());
        return toProfileDTO(profileRepository.save(profile));
    }

    private ProfileDTO toProfileDTO(Profile profile) {
        return new ProfileDTO(
                profile.getId(),
                profile.getUserId(),
                profile.getName(),
                profile.getEmail(),
                profile.getPhone(),
                profile.getType()
        );
    }

}
