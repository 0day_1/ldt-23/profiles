package ru.day.ltd.profiles.controller;

import lombok.extern.slf4j.Slf4j;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import ru.day.ltd.profiles.data.dto.ProfileNewDTO;
import ru.day.ltd.profiles.service.ProfileService;

import java.util.UUID;

@Slf4j
@RestController
@RequestMapping("api/v1/profile")
public class ProfileController {

    private final ProfileService profileService;


    public ProfileController(ProfileService profileService) {
        this.profileService = profileService;
    }

    @GetMapping("")
    public ResponseEntity<?> getProfile(@RequestParam(required = false) UUID userId) {
        try {
            return ResponseEntity.ok(profileService.getByUserId(userId));
        } catch (IllegalArgumentException e) {
            log.error("Can't get profile", e);
            return ResponseEntity.badRequest().body(e.getMessage());
        }
    }

    @GetMapping("/new")
    public ResponseEntity<?> createNewProfile(@RequestBody ProfileNewDTO profileNewDTO) {
        try {
            return ResponseEntity.ok(profileService.createNewProfile(profileNewDTO));
        } catch (RuntimeException e) {
            log.error("Can't create profile", e);
            return ResponseEntity.internalServerError().body(e.getMessage());
        }
    }
}
